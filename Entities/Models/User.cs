﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Models
{
    [Table("Users", Schema = "dbo")]
    public class User : BaseModel
    {
        public User()
        {
        }
        [Required(ErrorMessage = "Username cant be null.")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "Password cant be null.")]
        public string Password { get; set; }
        public UserType Type { get; set; }
        public string EmailAddress { get; set; }
    }
    public enum UserType
    {
        Customer = 1,
        MainBarber = 2,
        Barber = 3
    }
}
