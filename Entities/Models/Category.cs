﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Models
{
    [Table("Categories", Schema = "dbo")]
    public class Category : BaseModel
    {
        public Category()
        {
        }
        [Required(ErrorMessage = "Category title cant be null.")]
        public string Title { get; set; }
    }
}
