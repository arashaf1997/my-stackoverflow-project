﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Models
{
    [Table("Tags", Schema = "dbo")]
    public class Tag : BaseModel
    {
        public Tag()
        {
        }

        public Tag(string title)
        {
            Title = title;
        }
        [Required(ErrorMessage = "Tag title cant be null.")]
        public string Title { get; set; }
        public string Details { get; set; }
    }
}
