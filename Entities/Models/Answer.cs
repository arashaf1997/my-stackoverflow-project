﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Models
{
    [Table("Answers",Schema ="dbo")]
    public class Answer : BaseModel
    {
        public Answer()
        {
        }
        public string Text { get; set; }

        [ForeignKey(nameof(BarberShop))]
        public int BarberShopId { get; set; }
        public BarberShop BarberShop { get; set; }

        [ForeignKey(nameof(User))]
        public int UserId { get; set; }
        public virtual User User { get; set; }
    }
}
