﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Models
{
    [Table("ObjectTags", Schema = "dbo")]
    public class ObjectTag : BaseModel
    {
        public ObjectTag()
        {
        }

        [ForeignKey(nameof(Tag))]
        public int TagId { get; set; }
        public virtual Tag Tag { get; set; }

        [ForeignKey(nameof(BarberShop))]
        public int? BarberShopId { get; set; }
        public virtual BarberShop BarberShop { get; set; }

        [ForeignKey(nameof(Answer))]
        public int? AnswerId { get; set; }
        public virtual Answer Answer { get; set; }
    }
}
