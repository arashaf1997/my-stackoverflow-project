﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Models
{
    [Table("Comments", Schema = "dbo")]
    public class Comment : BaseModel
    {
        public Comment()
        {
        }

        public Comment(int userId, int? barberShopId, int? answerId, bool isLike)
        {
            UserId = userId;
            IsLike = isLike;
            BarberShopId = barberShopId;
            AnswerId = answerId;
        }

        [ForeignKey(nameof(User))]
        public int UserId { get; set; }
        public virtual User User { get; set; }

        [ForeignKey(nameof(BarberShop))]
        public int? BarberShopId { get; set; }
        public virtual BarberShop BarberShop { get; set; }

        [ForeignKey(nameof(Answer))]
        public int? AnswerId { get; set; }
        public virtual Answer Answer { get; set; }

        public bool IsLike { get; set; }
    }
}
