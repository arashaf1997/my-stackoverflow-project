﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.Models
{
    [Table("BarberShops", Schema = "dbo")]
    public class BarberShop : BaseModel
    {
        public BarberShop()
        {
        }

        public BarberShop(string title, string text , int userId , int? categoryId = null)
        {
            Title = title;
            Text = text;
            UserId = userId;
            CategoryId = categoryId;
            InsertTime = DateTime.Now;
        }
        public string Title { get; set; }
        public string Text { get; set; }
        
        [ForeignKey(nameof(User))]
        public int? UserId { get; set; }
        public User User { get; set; }

        [ForeignKey(nameof(Category))]
        public int? CategoryId { get; set; }
        public Category Category { get; set; }

        [ForeignKey(nameof(CorrectAnswer))]
        public int? CorrectAnswerId { get; set; }
        public Answer CorrectAnswer { get; set; }
    }
}
