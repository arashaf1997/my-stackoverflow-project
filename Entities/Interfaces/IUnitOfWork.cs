﻿using System;

namespace Entities.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IBarberShopsRepository BarberShops { get; }
        ICategoriesRepository Categories { get; }
        ITagsRepository Tags { get; }
        IAnswersRepository Answers { get; }
        IUsersRepository Users { get; }
        IObjectTagRepository ObjectTags { get; }
        ICommentsRepository Comments { get; }

        int Complete();
    }
}
