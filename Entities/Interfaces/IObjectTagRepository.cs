﻿using Entities.Models;
using System.Collections.Generic;

namespace Entities.Interfaces
{
    public interface IObjectTagRepository : IGenericRepository<ObjectTag>
    {
        void Set(List<ObjectTag> objectTags, bool isBarberShop);
    }
}
