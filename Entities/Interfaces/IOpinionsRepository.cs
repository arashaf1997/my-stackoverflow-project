﻿using Entities.Models;
using System;
using System.Collections.Generic;

namespace Entities.Interfaces
{
    public interface ICommentsRepository : IGenericRepository<Comment>
    {
        IEnumerable<Comment> GetCommentsOfBarberShops(int barberShopId);
        void RemoveCommentsOfObject(int? barberShopId, int? answerId);
        void Set(Comment comment);
        Dictionary<string,int> GetObjectComments(int objectId, bool isBarberShop);
    }
}
