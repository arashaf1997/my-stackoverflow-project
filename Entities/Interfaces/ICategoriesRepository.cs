﻿using Entities.Models;
using System.Collections.Generic;
using System.Linq;

namespace Entities.Interfaces
{
    public interface ICategoriesRepository : IGenericRepository<Category>
    {
        IQueryable<Category> GetFilteredCategories(string searchText);
        IQueryable<Category> GetAllCategories();

    }
}
