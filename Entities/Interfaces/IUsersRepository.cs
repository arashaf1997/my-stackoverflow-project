﻿using Entities.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Entities.Interfaces
{
    public interface IUsersRepository : IGenericRepository<User>
    {
        IQueryable<User> GetFilteredUsers(string searchText);
    }
}
