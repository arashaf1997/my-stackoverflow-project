﻿using Entities.Models;
using System.Collections.Generic;
using System.Linq;

namespace Entities.Interfaces
{
    public interface IBarberShopsRepository : IGenericRepository<BarberShop>
    {
        IQueryable<BarberShop> GetFilteredBarberShops(string searchText, int? categoryId, int? userId);
    }
}
