﻿using Entities.Models;
using System.Collections.Generic;
using System.Linq;

namespace Entities.Interfaces
{
    public interface IAnswersRepository : IGenericRepository<Answer>
    {
        IQueryable<Answer> GetAnswersOfBarberShop(int? barberShopId);
        void RemoveAnswersOfBarberShop(int id);
    }
}
