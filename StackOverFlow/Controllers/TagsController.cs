﻿using Entities.Interfaces;
using Entities.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using StackOverFlow.Models.Dtos.TagDtos;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StackOverFlow.Controllers
{
    public class TagsController : BaseController
    {
        public TagsController(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        [HttpGet]
        public List<TagDto> Get([FromQuery] string searchText = null)
        {
            if (searchText != null)
                return unitOfWork.Tags.GetFilteredTags(searchText).Select(tag => new TagDto { Title = tag.Title, Details = tag.Details }).ToList();
            else
                return unitOfWork.Tags.GetAll().Select(tag => new TagDto { Title = tag.Title, Details = tag.Details }).ToList();
        }

        [HttpPost]
        public void Add([FromQuery] string tagTitle)
        {
            var tag = new Tag
            {
                Title = tagTitle
            };
            unitOfWork.Tags.Add(tag);
            unitOfWork.Complete();
        }

        [HttpDelete("{tagId}")]
        public void Delete(int tagId)
        {
            var tag = unitOfWork.Tags.GetById(tagId);
            if (tag == null)
                throw new Exception();

            unitOfWork.Tags.Remove(tag);
            unitOfWork.Complete();
        }

        [HttpPut]
        public void Update([FromBody] Tag tagToUpdate)
        {
            var tag = unitOfWork.Tags.GetById(tagToUpdate.Id);
           
            if (tag == null)
                throw new Exception();

            tag.Title = tagToUpdate.Title;
            tag.Details = tagToUpdate.Details;
            unitOfWork.Complete();
        }
    }
}
