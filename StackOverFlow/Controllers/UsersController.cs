﻿using Entities.Interfaces;
using Entities.Models;
using Microsoft.AspNetCore.Mvc;
using StackOverFlow.Models.Dtos.UserDto;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace StackOverFlow.Controllers
{
    public class UsersController : BaseController
    {
        public UsersController(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        [HttpGet]
        public IEnumerable<UserDto> Get([FromQuery] string searchText = null)
        {
            if (searchText != null)
            {
                var filteredUsers = unitOfWork.Users.GetFilteredUsers(searchText).Select(user => new UserDto
                {
                    Id = user.Id,
                    EmailAddress = user.EmailAddress,
                    Username = user.UserName
                });
                return filteredUsers;
            }
            else
            {
                var users = unitOfWork.Users.GetAll().Select(user => new UserDto
                {
                    Id = user.Id,
                    EmailAddress = user.EmailAddress,
                    Username = user.UserName
                });
                return users;
            }
        }
        [HttpGet("{userId}")]
        public UserDto Get(int userId)
        {
            var user = unitOfWork.Users.GetById(userId);
            if (user == null)
                throw new Exception();

            var userDto = new UserDto
            {
                Id = user.Id,
                EmailAddress = user.EmailAddress,
                Username = user.UserName
            };
            return userDto;
        }
        [HttpPost]
        public void Add([FromBody] CreationUserDto userDto)
        {
            var user = new User
            {
                UserName = userDto.Username,
                EmailAddress = userDto.EmailAddress,
                Password = userDto.Password,
                InsertTime = DateTime.Now
            };
            unitOfWork.Users.Add(user);
            unitOfWork.Complete();
        }

        [HttpDelete("{userId}")]
        public void Delete(int userId)
        {
            var user = unitOfWork.Users.GetById(userId);
            if (user == null)
                throw new Exception();

            unitOfWork.Users.Remove(user);
            unitOfWork.Complete();
        }

        [HttpPut]
        public void Update([FromBody] UpdationUserDto updationUserDto)
        {
            var user = unitOfWork.Users.GetById(updationUserDto.Id);
            if (user == null)
                throw new Exception();

            user.EmailAddress = updationUserDto.EmailAddress;
            user.UserName = updationUserDto.Username;
            user.Password = updationUserDto.Password;
            user.EditTime = DateTime.Now;
            unitOfWork.Complete();
        }

    }
}
