﻿using Entities.Interfaces;
using Entities.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using StackOverFlow.Models.Dtos.AnswerDtos;
using StackOverFlow.Models.Dtos.BarberShopDtos;
using StackOverFlow.Models.Dtos.UserDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StackOverFlow.Controllers
{
    public class BarberShopsController : BaseController
    {
        public BarberShopsController(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        [HttpGet]
        public async Task<List<BarberShopDto>> Get([FromQuery] string searchText = null, int? categoryId = null, int? userId = null)
        {
            return await unitOfWork.BarberShops.GetFilteredBarberShops(searchText, categoryId, userId).Select(barberShop => new BarberShopDto { Id = barberShop.Id, Title = barberShop.Title, Text = barberShop.Text, UserId = (int)barberShop.UserId, CategoryId = (int)barberShop.CategoryId }).ToListAsync();
        }

        [HttpGet("{barberShopId}")]
        public BarberShopWithAnswersDto GetByIdWithAnswers(int barberShopId)
        {
            var barberShop = unitOfWork.BarberShops.GetById(barberShopId);
            var barberShopComments = unitOfWork.Comments.GetObjectComments(barberShopId, true);

            barberShopComments.TryGetValue("LikesCount", out int likesCount);
            barberShopComments.TryGetValue("DislikesCount", out int dislikesCount);

            var response = new BarberShopWithAnswersDto();
            response.BarberShop.Title = barberShop.Title;
            response.BarberShop.Text = barberShop.Text;
            response.BarberShop.EditTime = barberShop.InsertTime;
            response.BarberShop.CreateTime = barberShop.InsertTime;
            response.BarberShop.UserId = (int)barberShop.UserId;
            response.BarberShop.Id = barberShop.Id;
            response.BarberShop.LikesCount = likesCount;
            response.BarberShop.DislikesCount = dislikesCount;

            response.Answers = unitOfWork.Answers.GetAnswersOfBarberShop(barberShopId).Select(a => new AnswerDto
            {
                Id = a.Id,
                Text = a.Text,
                UserId = a.UserId,
                EditTime = a.EditTime,
                InsertTime = a.InsertTime,
                BarberShopId = a.BarberShopId
            }).ToList();

            //TOOD : Refactor
            foreach(var answer in response.Answers)
            {
                var answerComments = unitOfWork.Comments.GetObjectComments(answer.Id, false);
                dislikesCount = 0;
                likesCount = 0;
                answerComments.TryGetValue("LikesCount", out likesCount);
                answerComments.TryGetValue("DislikesCount", out dislikesCount);
                answer.LikesCount = likesCount;
                answer.DislikesCount = dislikesCount;
            }
            return response;
        }

        [HttpPost]
        public void Add([FromBody] CreationBarberShopDto createBarberShopDto)
        {
            BarberShop barberShop = new BarberShop(createBarberShopDto.Title, createBarberShopDto.Text, userId, createBarberShopDto.CategoryId);

            unitOfWork.BarberShops.Add(barberShop);
            unitOfWork.Complete();
            if (createBarberShopDto.Tags != null)
            {
                foreach (string tagTitle in createBarberShopDto.Tags)
                {
                    unitOfWork.Tags.CheckTagsToCreate(tagTitle);
                }
                unitOfWork.Complete();
                List<Tag> listOfTags = unitOfWork.Tags.GetListOfTags(createBarberShopDto.Tags);
                List<ObjectTag> barberShopTags = new List<ObjectTag>();
                foreach (Tag tag in listOfTags)
                {
                    barberShopTags.Add(new ObjectTag { TagId = tag.Id, BarberShopId = barberShop.Id });
                }
                unitOfWork.ObjectTags.AddRange(barberShopTags);
                unitOfWork.Complete();
            }
        }

        [HttpDelete("{barberShopId}")]
        public void Delete(int barberShopId)
        {
            var barberShop = unitOfWork.BarberShops.GetById(barberShopId);

            if (barberShop == null)
                throw new Exception();

            unitOfWork.Comments.RemoveCommentsOfObject(barberShopId, null);
            unitOfWork.Answers.RemoveAnswersOfBarberShop(barberShop.Id);
            unitOfWork.BarberShops.Remove(barberShop);
            unitOfWork.Complete();
        }

        [HttpPut]
        public void Update([FromBody] UpdationBarberShopDto updationBarberShopDto)
        {
            var BarberShop = unitOfWork.BarberShops.GetById(updationBarberShopDto.Id);
            if (BarberShop == null)
                throw new Exception();

            BarberShop.Title = updationBarberShopDto.Title;
            BarberShop.Text = updationBarberShopDto.Text;
            BarberShop.CategoryId = updationBarberShopDto.CategoryId;
            BarberShop.EditTime = DateTime.Now;

            if (updationBarberShopDto.Tags != null)
            {
                foreach (string tagTitle in updationBarberShopDto.Tags)
                {
                    unitOfWork.Tags.CheckTagsToCreate(tagTitle);
                }
                unitOfWork.Complete();
                List<Tag> listOfTags = unitOfWork.Tags.GetListOfTags(updationBarberShopDto.Tags);
                List<ObjectTag> barberShopTags = new List<ObjectTag>();
                foreach (Tag tag in listOfTags)
                {
                    barberShopTags.Add(new ObjectTag { TagId = tag.Id, BarberShopId = updationBarberShopDto.Id });
                }
                unitOfWork.ObjectTags.AddRange(barberShopTags);
            }
            unitOfWork.Complete();
        }
    }
}
