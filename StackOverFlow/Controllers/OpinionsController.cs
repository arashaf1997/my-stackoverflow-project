﻿using Entities.Interfaces;
using Entities.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using StackOverFlow.Models.Dtos.AnswerDtos;
using StackOverFlow.Models.Dtos.BarberShopDtos;
using StackOverFlow.Models.Dtos.UserDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StackOverFlow.Controllers
{
    public class CommentsController : BaseController
    {
        public CommentsController(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        [HttpPut("LikeOrDislike")]
        public void LikeOrDislike([FromQuery] bool isLike, int? answerId, int? barberShopId)
        {
            var comment = new Comment(userId, barberShopId, answerId, isLike);
            unitOfWork.Comments.Set(comment);
            unitOfWork.Complete();
        }

    }
}
