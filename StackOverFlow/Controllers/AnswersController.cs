﻿using Entities.Interfaces;
using Entities.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using StackOverFlow.Models.Dtos.AnswerDtos;
using StackOverFlow.Models.Dtos.AnswerDtos;
using StackOverFlow.Models.Dtos.UserDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StackOverFlow.Controllers
{
    public class AnswersController : BaseController
    {
        public AnswersController(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        [HttpGet]
        public List<AnswerDto> Get([FromQuery] int? barberShopId = null)
        {
            int dislikesCount = 0;
            int likesCount = 0;
            var answers = unitOfWork.Answers.GetAnswersOfBarberShop(barberShopId).Select(a => new AnswerDto
            {
                Id = a.Id,
                BarberShopId = a.BarberShopId,
                Text = a.Text,
                UserId = a.UserId,
                EditTime = a.EditTime,
                InsertTime = a.InsertTime
            }).ToList();

            foreach (var answer in answers)
            {
                var answerComments = unitOfWork.Comments.GetObjectComments(answer.Id, false);
                dislikesCount = 0;
                likesCount = 0;
                answerComments.TryGetValue("LikesCount", out likesCount);
                answerComments.TryGetValue("DislikesCount", out dislikesCount);
                answer.LikesCount = likesCount;
                answer.DislikesCount = dislikesCount;
            }

            return answers;
        }

        [HttpGet("GetById/{answerId}")]
        public AnswerDto GetById(int answerId)
        {
            var answer = unitOfWork.Answers.GetById(answerId);
            return new AnswerDto
            {
                Id = answer.Id,
                BarberShopId = answer.BarberShopId,
                Text = answer.Text,
                UserId = answer.UserId
            };
        }

        [HttpPost]
        public void Add([FromBody] CreationAnswerDto createAnswerDto)
        {
            var answer = new Answer
            {
                BarberShopId = createAnswerDto.BarberShopId,
                Text = createAnswerDto.Text,
                UserId = userId
            };
            unitOfWork.Answers.Add(answer);
            unitOfWork.Complete();
            if (createAnswerDto.Tags != null)
            {
                foreach (string tagTitle in createAnswerDto.Tags)
                {
                    unitOfWork.Tags.CheckTagsToCreate(tagTitle);
                }
                unitOfWork.Complete();
                List<Tag> listOfTags = unitOfWork.Tags.GetListOfTags(createAnswerDto.Tags);
                List<ObjectTag> barberShopTags = new List<ObjectTag>();
                foreach (Tag tag in listOfTags)
                {
                    barberShopTags.Add(new ObjectTag { TagId = tag.Id, AnswerId = answer.Id });
                }
                unitOfWork.ObjectTags.AddRange(barberShopTags);
                unitOfWork.Complete();
            }
        }

        [HttpDelete("{answerId}")]
        public void Delete(int answerId)
        {
            var answer = unitOfWork.Answers.GetById(answerId);
            if (answer == null)
                throw new ArgumentException();

            unitOfWork.Answers.Remove(answer);
            unitOfWork.Complete();
        }

        [HttpPut]
        public void Update([FromBody] UpdationAnswerDto updationAnswerDto)
        {
            var answer = unitOfWork.Answers.GetById(updationAnswerDto.Id);
            if (answer == null)
                throw new Exception();

            if (answer.UserId != userId)
                throw new Exception();

            answer.Text = updationAnswerDto.Text;
            answer.EditTime = DateTime.Now;
            if (updationAnswerDto.Tags != null)
            {
                foreach (string tagTitle in updationAnswerDto.Tags)
                {
                    unitOfWork.Tags.CheckTagsToCreate(tagTitle);
                }
                unitOfWork.Complete();
                List<Tag> listOfTags = unitOfWork.Tags.GetListOfTags(updationAnswerDto.Tags);
                List<ObjectTag> barberShopTags = new List<ObjectTag>();
                foreach (Tag tag in listOfTags)
                {
                    barberShopTags.Add(new ObjectTag { TagId = tag.Id, AnswerId = answer.Id });
                }
                unitOfWork.ObjectTags.Set(barberShopTags, false);
                unitOfWork.Complete();
            }
        }

        [HttpPut("SetAsTrue/{answerId}")]
        public void SetAsTrue(int answerId)
        {
            var answer = unitOfWork.Answers.GetById(answerId);
            if (answer == null)
                throw new Exception();

            if (answer.BarberShop.UserId != userId)
                throw new Exception();

            answer.BarberShop.CorrectAnswer = answer;
            unitOfWork.Complete();
        }
    }
}