﻿using Entities.Interfaces;
using Entities.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using StackOverFlow.Models.Dtos.CategoryDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StackOverFlow.Controllers
{
    public class CategoriesController : BaseController
    {
        public CategoriesController(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        [HttpGet]
        public async Task<List<CategoryDto>> Get([FromQuery] string searchText = null)
        {
            if (searchText != null)
            {
                return await unitOfWork.Categories.GetFilteredCategories(searchText).Select(t => new CategoryDto { Id = t.Id, Title = t.Title }).ToListAsync();
            }
            else
            {
                return await unitOfWork.Categories.GetAllCategories().Select(category => new CategoryDto { Id = category.Id, Title = category.Title }).ToListAsync();
            }
        }

        [HttpPost]
        public void Add([FromQuery] string CategoryTitle)
        {
            var category = new Category
            {
                Title = CategoryTitle
            };
            unitOfWork.Categories.Add(category);
            unitOfWork.Complete();
        }

        [HttpDelete("{categoryId}")]
        public void Delete(int categoryId)
        {
            var category = unitOfWork.Categories.GetById(categoryId);
            if (category == null)
                throw new Exception();

            unitOfWork.Categories.Remove(category);
            unitOfWork.Complete();
        }

        [HttpPut]
        public void Update([FromBody] Category categoryToUpdate)
        {
            var category = unitOfWork.Categories.GetById(categoryToUpdate.Id);
            if (category == null)
                throw new Exception();
         
            category.Title = categoryToUpdate.Title;
            unitOfWork.Complete();
        }
    }
}
