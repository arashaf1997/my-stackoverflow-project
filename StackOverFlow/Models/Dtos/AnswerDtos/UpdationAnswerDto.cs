﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StackOverFlow.Models.Dtos.AnswerDtos
{
    public class UpdationAnswerDto
    {
        public int Id { get; set; }
        [Required]
        public string Text { get; set; }
        public List<string> Tags { get; set; }
    }
}
