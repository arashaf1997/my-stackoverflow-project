﻿using StackOverFlow.Models.Dtos.UserDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StackOverFlow.Models.Dtos.AnswerDtos
{
    public class AnswerDto
    {
        public int Id { get; set; }
        public string Text{ get; set; }
        public int BarberShopId { get; set; }
        public int UserId { get; set; }
        public DateTime InsertTime { get; set; }
        public DateTime EditTime { get; set; }
        public int LikesCount { get; set; }
        public int DislikesCount { get; set; }

    }
}
