﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StackOverFlow.Models.Dtos.TagDtos
{
    public class TagDto
    {
        public string Title { get; set; }
        public string Details { get; set; }
    }
}
