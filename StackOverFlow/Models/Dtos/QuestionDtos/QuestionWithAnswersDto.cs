﻿using StackOverFlow.Models.Dtos.AnswerDtos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StackOverFlow.Models.Dtos.BarberShopDtos
{
    public class BarberShopWithAnswersDto
    {
        public BarberShopWithAnswersDto()
        {
            BarberShop = new BarberShopDto();
            Answers = new List<AnswerDto>();
        }
        public BarberShopDto BarberShop { get; set; }
        public List<AnswerDto> Answers { get; set; }
    }

}
