﻿using StackOverFlow.Models.Dtos.CategoryDto;
using StackOverFlow.Models.Dtos.TagDtos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StackOverFlow.Models.Dtos.BarberShopDtos
{
    public class BarberShopDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        //public GetCategoryDto Category { get; set; }
        public int CategoryId { get; set; }
        public List<TagDto> Tags {get;set;}
        public int UserId { get; set; }
        public DateTime EditTime { get; set; }
        public DateTime CreateTime { get; set; }
        public int LikesCount { get; set; }
        public int DislikesCount { get; set; }
    }
}
