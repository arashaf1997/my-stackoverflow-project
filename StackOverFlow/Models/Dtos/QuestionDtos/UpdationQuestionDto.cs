﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StackOverFlow.Models.Dtos.BarberShopDtos
{
    public class UpdationBarberShopDto
    {
        public int Id { get; set; }
        [MaxLength(5)]
        [Required]
        public string Title { get; set; }
        public string Text { get; set; }
        public int? CategoryId { get; set; }

        public List<string> Tags { get; set; }
    }
}
