﻿using Entities.Interfaces;
using Entities.Models;
using System.Collections.Generic;
using System.Linq;

namespace Data.Repositories
{
    public class TagsRepository : GenericRepository<Tag>, ITagsRepository
    {
        public TagsRepository(RepositoryContext context) : base(context)
        {
        }

        public void CheckTagsToCreate(string tagTitle)
        {
            if (!Context.Tags.Any(t => t.Title == tagTitle))
            {
                Context.Add(new Tag { Title = tagTitle });
            }
        }

        public IQueryable<Tag> GetFilteredTags(string searchText)
        {
            return Context.Tags.Where(t => t.Title.Contains(searchText));
        }

        public List<Tag> GetListOfTags(List<string> tags)
        {
            List<Tag> list = new List<Tag>();
            foreach (var tagTitle in tags)
            {
                list.Add(Context.Tags.Where(t => t.Title == tagTitle).FirstOrDefault());
            }
            return list;
        }
    }
}
