﻿using Entities.Interfaces;
using Entities.Models;
using System.Collections.Generic;
using System.Linq;

namespace Data.Repositories
{
    public class CategoriesRepository : GenericRepository<Category>, ICategoriesRepository
    {
        public CategoriesRepository(RepositoryContext context) : base(context)
        {
        }

        public IQueryable<Category> GetAllCategories()
        {
            return Context.Categories;
        }

        public IQueryable<Category> GetFilteredCategories(string searchText)
        {
            return Context.Categories.Where(t => t.Title.Contains(searchText));
        }
    }
}
