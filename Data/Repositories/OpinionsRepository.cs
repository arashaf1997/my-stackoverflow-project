﻿using Entities.Interfaces;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Data.Repositories
{
    public class CommentsRepository : GenericRepository<Comment>, ICommentsRepository
    {
        public CommentsRepository(RepositoryContext context) : base(context)
        {
        }

        public void Set(Comment comment)
        {
            Comment lastComment;

            if (comment.BarberShopId != null)
                lastComment = Context.Comments.FirstOrDefault(q => q.BarberShopId == comment.BarberShopId && q.UserId == comment.UserId);
            else
                lastComment = Context.Comments.FirstOrDefault(q => q.AnswerId == comment.AnswerId && q.UserId == comment.UserId);


            if (lastComment != null)
                lastComment.IsLike = comment.IsLike;
            else
            {
                comment.InsertTime = DateTime.Now;
                Context.Comments.Add(comment);
            }
        }

        public IEnumerable<Comment> GetCommentsOfBarberShops(int barberShopId)
        {
            var barberShopsComments = Context.Comments.Where(a => a.BarberShopId == barberShopId).ToList();
            return barberShopsComments;
        }

        public void RemoveCommentsOfObject(int? barberShopId, int? answerId)
        {
            if (barberShopId != null)
                Context.Comments.RemoveRange(Context.Comments.Where(q => q.BarberShopId == barberShopId));
            if (answerId != null)
                Context.Comments.RemoveRange(Context.Comments.Where(q => q.AnswerId == answerId));
        }

        public Dictionary<string, int> GetObjectComments(int objectId, bool isBarberShop)
        {
            int likesCount = 0;
            int dislikesCount = 0;

            var counter = new Dictionary<string, int>();

            if (isBarberShop)
            {
                if (!Context.Comments.Any(op => op.BarberShopId == objectId))
                {
                    counter.Add("LikesCount", likesCount);
                    counter.Add("DislikesCount", dislikesCount);
                    return counter;
                }
                dislikesCount = Context.Comments.Where(o => o.IsLike == false && o.BarberShopId == objectId).Count();
                likesCount = Context.Comments.Where(o => o.IsLike == true && o.BarberShopId == objectId).Count();
            }
            else
            {
                if (!Context.Comments.Any(op => op.AnswerId == objectId))
                {
                    counter.Add("LikesCount", likesCount);
                    counter.Add("DislikesCount", dislikesCount);
                    return counter;
                }
                dislikesCount = Context.Comments.Where(o => o.IsLike == false && o.AnswerId == objectId).Count();
                likesCount = Context.Comments.Where(o => o.IsLike == true && o.AnswerId == objectId).Count();
            }
            counter.Add("LikesCount", likesCount);
            counter.Add("DislikesCount", dislikesCount);
            return counter;
        }
    }
}
