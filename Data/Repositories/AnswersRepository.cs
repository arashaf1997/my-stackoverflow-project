﻿using Entities.Interfaces;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Data.Repositories
{
    public class AnswersRepository : GenericRepository<Answer>, IAnswersRepository
    {
        public AnswersRepository(RepositoryContext context) : base(context)
        {
        }

        public IQueryable<Answer> GetAnswersOfBarberShop(int? barberShopId)
        {
            if (barberShopId != null)
            {
                if (!Context.BarberShops.Any(q => q.Id == barberShopId))
                    throw new Exception();

                return Context.Answers.Where(a => a.BarberShopId == barberShopId);
            }
            else
                return Context.Answers;
        }

        public void RemoveAnswersOfBarberShop(int barberShopId)
        {
            var answersOfBarberShop = Context.Answers.Where(a => a.BarberShopId == barberShopId);
            Context.Answers.RemoveRange(answersOfBarberShop);
        }
    }
}
