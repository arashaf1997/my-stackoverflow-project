﻿using Entities.Interfaces;
using Entities.Models;
using System.Collections.Generic;
using System.Linq;

namespace Data.Repositories
{
    public class ObjectTagRepository : GenericRepository<ObjectTag>, IObjectTagRepository
    {
        public ObjectTagRepository(RepositoryContext context) : base(context)
        {
        }

        public void Set(List<ObjectTag> objectTags, bool isBarberShop)
        {
            List<ObjectTag> oldObjectTags = new List<ObjectTag>();
            if (isBarberShop)
            {
                oldObjectTags = Context.ObjectTag.Where(ot => ot.BarberShopId == objectTags[0].BarberShopId).ToList();
                Context.RemoveRange(oldObjectTags);

                Context.AddRange(objectTags);
            }
            else
            {
                oldObjectTags = Context.ObjectTag.Where(ot => ot.AnswerId == objectTags[0].AnswerId).ToList();
                Context.RemoveRange(oldObjectTags);

                Context.AddRange(objectTags);
            }
        }
    }
}
