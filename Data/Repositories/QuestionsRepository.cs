﻿using Entities.Interfaces;
using Entities.Models;
using System.Collections.Generic;
using System.Linq;

namespace Data.Repositories
{
    public class BarberShopsRepository : GenericRepository<BarberShop>, IBarberShopsRepository
    {
        public BarberShopsRepository(RepositoryContext context) : base(context)
        {
        }

        public IQueryable<BarberShop> GetFilteredBarberShops(string searchText, int? categoryId, int? userId)
        {
            return Context.BarberShops.Where(t => (((t.Title.Contains(searchText)) || t.Text.Contains(searchText)) || searchText == null)
                                          && (t.CategoryId == categoryId || categoryId == null)
                                          && (t.UserId == userId || userId == null));
        }
    }
}
