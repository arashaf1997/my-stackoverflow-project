﻿using Entities.Interfaces;
using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Data.Repositories
{
    public class UsersRepository : GenericRepository<User>, IUsersRepository
    {
        public UsersRepository(RepositoryContext context) : base(context)
        {
        }

        public IQueryable<User> GetFilteredUsers(string searchText)
        {
            return Context.Users.Where(t => t.UserName.Contains(searchText) || t.EmailAddress.Contains(searchText));
        }
    }
}
