﻿using Data.Repositories;
using Entities.Interfaces;

namespace Data.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly RepositoryContext context;
        public UnitOfWork(RepositoryContext context)
        {
            this.context = context;
            BarberShops = new BarberShopsRepository(this.context);
            Categories = new CategoriesRepository(this.context);
            Tags = new TagsRepository(this.context);
            Users = new UsersRepository(this.context);
            Answers = new AnswersRepository(this.context);
            ObjectTags = new ObjectTagRepository(this.context);
            Comments = new CommentsRepository(this.context);
        }
        public IBarberShopsRepository BarberShops { get; private set; }
        public ICategoriesRepository Categories { get; private set; }
        public ITagsRepository Tags { get; private set; }
        public IAnswersRepository Answers { get; private set; }
        public IUsersRepository Users { get; private set; }
        public IObjectTagRepository ObjectTags { get; private set; }
        public ICommentsRepository Comments { get; private set; }

        public int Complete()
        {
            return this.context.SaveChanges();
        }
        public void Dispose()
        {
            this.context.Dispose();
        }
    }
}
